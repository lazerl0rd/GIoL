# Game installation instructions

Minimal requirements:

 * Any installation of `wine` >= 5.3 (sooner or later)

Highly recommended:

 * Vulkan-capable graphics card (["How to check?"](TROUBLESHOOTING.md#graphics-bugsissues))


### Table of contents

* [Lutris](#lutris)
* [Steam](#steam)
* [Custom](#custom)


#### Lutris

1. Download the Windows installer from the official site
2. Add the installer binary to your library
3. Set up a wineprefix for the game
    * This is where the game will be installed. 40+ GiB of free space is highly recommended.
    * Grab at least `lutris-6.4` for the Wine runner.
4. Install the components the game needs to run flawlessly
    * Open Winetricks via the context menu
    * Install the Windows component `faudio` and the font `corefonts`.
    * Enable DXVK in `Configuration` -> `Runner options` (1.7.3 or newer is recommended)
5. Launch the downloaded installer .exe
    * Use the `Run EXE inside wine prefix` menu action for this.
    * Go with the default settings, and wait for it to download the game.
    * **Once it is done, do not press launch.** Exit the launcher.
6. Apply the patch script: ["how?"](README.md)
7. Play the game
    * In `Configuration` -> `Game options` -> `Executable` path, browse and select
      `launcher.bat` which you can find in the same directory as `GenshinImpact.exe`
    * Launch the game

**Important note:**

After setup you might have multiple Wine versions and multiple WINEPREFIX directories
on your system. Hence running the `wine` command from your terminal will use another
environment than Lutris does.

To open a command prompt which uses the correct environment, do either:

 * Games -> Wine options (see bottom status bar or left side) -> `Wine console`
 * Change the `Executable` path to `cmd.exe`


#### Steam

1. Download the official launcher
2. "Add non-Steam game..." -> Show all files -> Select the launcher executable -> Done
3. Setup proton (if not done using global settings)
    * Navigate: Gear icon -> "Settings" -> "Compatibility"
    * Check "Force compatibility layer ..."
    * Select Proton 5.13 or newer
4. Install the game. Select a path that you can find later on!
5. Apply the patch script: ["how?"](README.md)
6. Update the target. Gear icon -> "Settings":
    * Target: `explorer.exe`
    * Execution directory: `/path/to/your/gi/installation/`
    * Start options: `/desktop=anyname,1920x1080 cmd /c launcher.bat`
    * Adjust the screen resolution above, if necessary.
7. Play.


#### Custom

Script-based approach to use any Wine version on any WINEPREFIX.
If you do not understand what each step is supposed to do, use Lutris instead.

For the final game launch script after the prefix setup, use steps 1, 2 and 6.

	# 1. Specify the path to wine(64)
	#    This can be either system-wide or a custom installation
	WINE="wine64"
	#WINE="$HOME/.local/share/Steam/steamapps/common/Proton 5.13/dist/bin/wine64"
	
	# 2. Specify a WINEPREFIX
	export WINEPREFIX="/absolute/path/to/.prefix/"
	
	# 3. Create or update WINEPREFIX
	"$WINE" wineboot -u
	
	# 4. Install DXVK (v1.9.0, for Wine 5.14+)
	#    Use the newest Winetricks script from GitHub
	export WINE
	sh winetricks dxvk190
	
	# 5. Install the game (if not already done)
	"$WINE" "/path/to/installer.exe"
	
	# 6. Game launch
	cd "/path/to/Genshin Impact Game"
	export WINEESYNC=1  # requires staging or Proton, see TWEAKS.md
	"$WINE" explorer /desktop=anyname,1920x1080 cmd /c launcher.bat
	
	# 7. Game update
	#    See `updater/README.md` or attempt to use the official launcher:
	"$WINE" "/path/to/official/launcher.exe"
