#!/usr/bin/env bash

ANCHOR="GenshinImpact.exe"

if [[ ! -e "$ANCHOR" ]]; then
	echo "==> GenshinImpact executable not found. Wrong directory?"
	exit 1
fi

# Restore files that have a backup
FILE="UnityPlayer.dll"
CEXE="GenshinImpact_Data/upload_crash.exe"
XLUA="GenshinImpact_Data/Plugins/xlua.dll"

# macOS has a different version of stat
if [ $(uname) = "Darwin" ]; then
	STATFLAGS="-f %a"
else
	STATFLAGS="-c %Y"
fi

# Reference timestamp to avoid restoring old/invalid backups
ref_timestamp=($(stat $STATFLAGS "$ANCHOR"))

restore_file() {
	# $1 -> File name
	# $2 -> Human readable description

	if [[ ! -e "$1.bak" ]]; then
		echo "--- Nothing to restore for '$1' (no .bak file)"
		return 0
	fi

	# Do not restore backups from old game versions
	difftime=$(( $ref_timestamp - $(stat $STATFLAGS "$1.bak") ))
	# Strip negative sign is equal to abs()
	if [[ ${difftime#-} -gt 3600 ]]; then
		echo "==> ERROR: Backup '$1.bak' is older than the current game version. Ignoring."
		return 1
	fi

	# Restore from backup
	mv -f "$1.bak" "$1"
	echo "--- Restored: $2"
	return 0
}

restore_file "$FILE" "$FILE"
restore_file "$XLUA" "$XLUA"

if [[ -e "$CEXE" ]]; then
	echo "--- Crash reporter already exists"
else
	restore_file "$CEXE" "Crash reporter"
fi


echo "--- Removing all newly added files"
# The error messages for inexistent files are intended
rm "launcher.bat"
rm "mhyprot2_running.reg"
# xlua patch
rm "no message"

# dxvk files
rm *.dxvk-cache
rm *_d3d9.log
rm *_d3d11.log
rm *_dxgi.log


echo "==> Patch reverted."
exit 0
